function initApp() {
    var txtEmail = document.getElementById('userEmail');
    var txtPassword = document.getElementById('userPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btnGoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(){
            document.location.href = "startpage.html";
        }).catch(function(error) {
            alert('Error!! '+error.message);
            txtEmail.value = null;
            txtPassword.value = null;
        });
    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
            document.location.href = "startpage.html";
            }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
            alert('Error!! '+error.message);
            });
    });

    btnSignUp.addEventListener('click', function () {        
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(){
            alert('Success!!');
            txtEmail.value = null;
            txtPassword.value = null;
        }).catch(function(error) {
            alert('Error!! '+error.message);
            txtEmail.value = null;
            txtPassword.value = null;
        });
    });
}
window.onload = function () {
    initApp();
};