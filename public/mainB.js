function init() {
    var user_email = '';
    post_btn = document.getElementById('postBtn');
    post_txt = document.getElementById('posSom');
    var postsRef = firebase.database().ref('forumB_list');
    var postsRef_com = firebase.database().ref('forumB_list_comment_list');
    var showUser = document.getElementById('userName');
    var logoutbtn = document.getElementById('logoutBtn');
    var comList = [];
    var comKey = [];
    firebase.auth().onAuthStateChanged(function (user) {
        //var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            showUser.innerHTML = user_email;
            logoutbtn.addEventListener('click', function (){
                firebase.auth().signOut().then(function() {
                    alert('您已登出，請重新登入');
                    document.location.href = "mainB.html";
            })
            })
        } else {
            user_email = "Guest";
            logoutbtn.innerHTML = "登入";
            logoutbtn.addEventListener('click', function (){
                    document.location.href = "index.html";
            }).catch(function(e){
                alert('Error');
            })
        }
    });
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var today=new Date();
            var currentDateTime =today.getFullYear()+'年'+(today.getMonth()+1)+'月'+today.getDate()+'日('+today.getHours()+':'+today.getMinutes()+')';
            postsRef.push().set({
                email: user_email,
                data: post_txt.value,
                time: currentDateTime
            });
            console.log(currentDateTime);
            post_txt.value = "";
        }
    });
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(e){
                var pos = document.createElement("DIV");
                pos.className = "container bor"
                pos.innerHTML = "<h2>" + e.val().email + "</h2><h5>" + e.val().time + '</h5><h4>' + e.val().data + "</h4>";;
                document.body.appendChild(pos);
                var comL = document.createElement("DIV");
                pos.appendChild(comL);
                comList.push(comL);
                comKey.push(e.key);

                var comDiv = document.createElement("DIV");
                comDiv.className = "form-group";
                var comTexA = document.createElement("TEXTAREA");
                comTexA.rows = "2";
                comTexA.className = "form-control textA";
                comTexA.setAttribute("placeholder", "留言");
                var comBtn = document.createElement("BUTTON");
                comBtn.setAttribute("type", "button");
                comBtn.className = "btn btn-primary btn-sm";
                comBtn.innerHTML = "確認";
                comBtn.addEventListener('click', function () {
                    if (comTexA.value != "") {
                        var today=new Date();
                        var currentDateTime =today.getFullYear()+'年'+(today.getMonth()+1)+'月'+today.getDate()+'日('+today.getHours()+':'+today.getMinutes()+')';
                        var comStr = ">>" + user_email + ":" + comTexA.value;
                        postsRef_com.push().set({
                            CK: e.key,
                            comment: comStr,
                            time: currentDateTime
                        });
            
                        comTexA.value = "";
                    }
                });
                comDiv.appendChild(comTexA);
                comDiv.appendChild(comBtn);
                pos.appendChild(comDiv);
                first_count++;
            })

            postsRef.on('child_added', function(data) {
                second_count++;
                if(first_count < second_count){
                    var newpos = document.createElement("DIV");
                    newpos.className = "container bor"
                    newpos.innerHTML = "<h2>" + data.val().email + "</h2><h5>" + data.val().time + '</h5><h4>' + data.val().data + "</h4>";
                    document.body.appendChild(newpos);
                    var newcomL = document.createElement("DIV");
                    newpos.appendChild(newcomL);
                    comList.push(newcomL);
                    comKey.push(data.key);

                    var newcomDiv = document.createElement("DIV");
                    newcomDiv.className = "form-group";
                    var newcomTexA = document.createElement("TEXTAREA");
                    newcomTexA.rows = "2";
                    newcomTexA.className = "form-control textA";
                    newcomTexA.setAttribute("placeholder", "留言");
                    var newcomBtn = document.createElement("BUTTON");
                    newcomBtn.setAttribute("type", "button");
                    newcomBtn.className = "btn btn-primary btn-sm";
                    newcomBtn.innerHTML = "確認";
                    newcomBtn.addEventListener('click', function () {
                        if (newcomTexA.value != "") {
                            var newcomStr = ">>" + user_email + ":" + newcomTexA.value;
                            var today=new Date();
                            var currentDateTime =today.getFullYear()+'年'+(today.getMonth()+1)+'月'+today.getDate()+'日('+today.getHours()+':'+today.getMinutes()+')';
                            postsRef_com.push().set({
                                CK: data.key,
                                comment: newcomStr,
                                time: currentDateTime 
                            });
                
                            newcomTexA.value = "";
                        }
                    });
                    newcomDiv.appendChild(newcomTexA);
                    newcomDiv.appendChild(newcomBtn);
                    newpos.appendChild(newcomDiv);
                    first_count++;
                }    
            });
        })
        .catch(e => console.log(e.message));
        
        var first_count_com = 0;
        var second_count_com = 0;   
        
        postsRef_com.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(e){
                var com = document.createElement("P");
                com.innerHTML = e.val().comment + "<br>" + e.val().time;
                for(i=0;i<comKey.length;i++){
                    if(e.val().CK == comKey[i]){
                        comList[i].appendChild(com);
                    }
                }
                first_count_com++;
            })
            postsRef_com.on('child_added', function(data) {
                second_count_com++;
                if(first_count_com < second_count_com){
                    var newcom = document.createElement("P");
                    newcom.innerHTML = data.val().comment + "<br>" + data.val().time;
                    for(j=0;j<comKey.length;j++){
                        if(data.val().CK == comKey[j]){
                            comList[j].appendChild(newcom);
                        }
                    }
                    first_count_com++;
                }    
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};

function switchToA() {
    document.location.href = "mainA.html";
}