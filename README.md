# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Forum
* Key functions (add/delete)
    1. Sign in
    2. Sign Up
    3. Not just based on third-party web service like google(可以用郵件創帳號登入)
    4. RWD
    5. 發布文章
    6. 文章下留言
* Other functions (add/delete)
    1. 顯示留言和文章時間時間
    2. CSS動畫
    3. [xxxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://myweb-c9d1c.firebaseapp.com

# Components Description : 
1. Membership Mechanism/  Sign Up/In with Google: 一開始進到網站，需要先登入或註冊帳號，可以選擇用郵件註冊或Gmail登入，如果註冊帳號時mail格式不對或密碼沒超過六個字將無法成功創立帳號，此時會跳出通知提醒
3. Host on my Firebase page: 把網站deploy到firebase上
4. database read/write: 用ref.on跟ref.once來盡興讀取，用Ref.push().set來寫進資料
5. RWD: 能在大小視窗正常運行網站
6. Forum key function / CSS animation: 登入之後，會跳到一個二選一的介面，可以選擇要進入哪一個版。進入討論版後，左上角可以看到是哪一個版，右上角可以看到使用者的email，在email下面有兩個按鈕，第一個按下後會切換到另一個版，上面又用CSS做換顏色的動畫。第二個按鈕式登出鈕，按下後仍可以看到貼文和流言，只是會變成訪客模式，輸入留言及貼文後的使用者信箱將會變成GUEST，可以匿名留言。在登出後會跳出提醒提醒使用者再次登入，而登出鈕會變成登入，如果按下會跳到登入畫面。在下面有一個文字輸入框，中間可以寫下要發出的文章內容，按下發佈後貼文會即時更新在網頁最下方，內容有發佈者的email，內文跟發文時間，在每一則貼文下方都有一個輸入框跟確認按鈕，按下確認後便可發佈留言，留言會即時更新在貼文下方，會有留言內容以及留言時間。
...

# Other Functions Description(1~10%) : 
1. 留言時間 : 在使用者發佈貼文及留言時，網站會同時將時間資訊傳到資料庫進行儲存，當網站在讀取資料時會將時間一併讀出並顯示。
2. 訪客登入 : 在帳號登入後可以按登出鈕來匿名留言。
...

## Security Report (Optional)
